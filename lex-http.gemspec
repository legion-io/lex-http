require_relative 'lib/legion/extensions/http/version'

Gem::Specification.new do |spec|
  spec.name          = 'lex-http'
  spec.version       = Legion::Extensions::Http::VERSION
  spec.authors       = ['Esity']
  spec.email         = ['matthewdiverson@gmail.com']

  spec.summary       = 'LEX HTTP'
  spec.description   = 'Connections Legion to any HTTP source'
  spec.homepage      = 'https://bitbucket.org/legion-io/lex-http/src'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.5.0')

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://bitbucket.org/legion-io/lex-http/src'
  spec.metadata['changelog_uri'] = 'https://bitbucket.org/legion-io/lex-http/src/master/CHANGELOG.md'

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.require_paths = ['lib']

  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rubocop'

  spec.add_dependency 'faraday'
  spec.add_dependency 'faraday_middleware'
  spec.add_dependency 'multi_json'
  spec.add_dependency 'multi_xml'
end
